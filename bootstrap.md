# Bootstrap Sec/Non-sec example client

Bootstrap is the process of provisioning the Device Management Client to a state where it is able to initiate a management session to a new Device Management Server.

The example client has a device management server information in the device, so that they can automatically connect to the dm-server. 

## The Bootstrap Flow

> * The Device (Device Management client) has boostrap server URL and endpoint
> * The Device has no Device Management server or cannot connect to the one, then the device initiates a bootstrap
> * The Device sends a bootstrap request using bootstrap server URL and endpoint
> * In turn, the bootstrap server sends Device Management sercer URL and credentials to device
> * The Device then registers to the Device Management Server


## STEP 1: Create device model 

Choose either **LWM2M Bootstrap. Not secured connection parameters** for non secure connection 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/Bsnonsec.png)

**LWM2M Bootstrap. DTLS connection. Pre-shared keys mode** for secured connection. When PSK mode is selected, we assume the device is also under PSK mode and vice versa.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/BsSec.png)

When both modes are selected the example client will provide both PSK and NO-Sec info in the device.



## STEP 2: Deploy the device using the model that is created

Deploy the device with the model that is created. Devices->Single->Model(Select the model to deploy from the Tab that opens)

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/device_deployed.png)

## STEP 3: Run the demo client

 Download the demo client from [loop-client.jar](https://bitbucket.org/litmusloopdocs/loopcloud-docs/raw/adf2d11ae89a3193d4de9708dc2b8e09757b1fe6/lwm2mNosec/loopdm-client.jar).
 
 Open terminal and use the following command
 
```
  java -jar loopdm-client.jar -bs -config [path-to-the-config-file]
  
```
 
 The -bs option is essential, this option is to tell the client that we are under boostrap mode. Wait for a few seconds for the client to register to the server. It will take a while because,the client will first connect to the 
 boostrap server to retrieve all the device management information. 
 

```
usage:java -jar loopdm-client.jar [OPTION]

-h,--help         Display help information
-bs               First connect to the bootstrap server
-config <arg>     Set the configuration file for the Client
-port <arg>       Set the local CoAP port of the Client
                  Default: A valid port value is between 0 and 65535
                  
```


## STEP 4: Green status of the device indicates that the device is successfully registered. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/status.png)

The Resources can be Read/Write/Observe/Execute using lwm2m as per the OMA standards.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/explore.png)


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/Read.png)    Read - To read the object and its resources once, example sensor value


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/Write.png)   Write- To write the object and its resourcs, example Threshold value


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/execute.png) Execute- To Execute the oject, example Reboot a device


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/observe.png) Observe- To observe (Read) the constantly changing value eg- sesnor values

*Note: Observe mode is off by default and needs to be turned on to observe*


All the events of adding a device, writing to a device or reading from a device etc. can be seen from the LOG tab under each device section.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//lwm2mNosec/log.png)