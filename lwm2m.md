# Lwm2m No-secure/Secure connection example

OMA **Lightweight M2M** is a protocol from the Open Mobile Alliance for M2M or IoT device management. Lightweight M2M enabler defines the application layer communication protocol between a LWM2M Server and a LWM2M Client, which is located in a LWM2M Device.
The example features Manage registration, update, disconnect devices, collect/update and observe device data.

Below are the Steps to create a model and deploy a single device using the either secure connection or non-secure connection.

*NOTE: Make sure which model you deploy as there is no special command for secure DTLS connection, both the models work with same java command*

## STEP 1:

Create Device Model under Company->Project->Models, by selecting the **LWM2M.Not secured connection parameters** for non secure connection

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//lwm2mNosec/model.png)


Create Device Model under Company->Project->Models, by selecting the **LWM2M.DTLS connection.Pre-shared keys mode** for  secure connection. The model contains the **lwm2mPSKIdentity** and the **lwm2mPSKValue**

![alte text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//lwm2mNosec/modelsec.png)

## STEP 2:

Deploy the device by selecting the model that was created. Devices->Single->Select the model which was created above.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//lwm2mNosec/device_deployed.png)

## STEP 3:

Download the [loopdm-client.jar](https://bitbucket.org/litmusloopdocs/loopcloud-docs/raw/d9857ff5cd1adbc756f93cb7d4dbf4f3dbd04881/lwm2mNosec/loopdm-client.jar) and run the command to connect the client as per the following usage

```
usage:java -jar loopdm-client.jar [OPTION]

-h,--help         Display help information
-bs               First connect to the bootstrap server
-config <arg>     Set the configuration file for the Client
-port <arg>       Set the local CoAP port of the Client
                  Default: A valid port value is between 0 and 65535
                                    
```

The config file is downloaded when the device is deployed. The user need to define the path of the downloaded file (or have the cleint and the json in the same folder) for the demo client to work, for example

```
java -jar loopdm-client.jar -config test_6vt5adacaecadasdas.json

```

## STEP 4: Green status of the device indicates that the device is successfully registered. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/status.png)

The Resources can be Read/Write/Observe/Execute using lwm2m as per the OMA standards.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/explore.png)


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/Read.png)    Read - To read the object and its resources once, example sensor value


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/Write.png)   Write- To write the object and its resourcs, example Threshold value


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/execute.png) Execute- To Execute the oject, example Reboot a device


![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/lwm2mNosec/observe.png) Observe- To observe (Read) the constantly changing value eg- sesnor values

*Note: Observe mode is off by default and needs to be turned on to observe*


All the events of adding a device, writing to a device or reading from a device etc. can be seen from the LOG tab under each device section.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//lwm2mNosec/log.png)