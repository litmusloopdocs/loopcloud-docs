# Getting Started with Dummy Device script   

The script can be downloaded from [here](https://bitbucket.org/litmusloopdocs/loopcloud-docs/raw/master/Demo/oma_test.py)

Install mqtt library using

```
pip install paho-mqtt

```

Execute the script using 

```
python oma_test.py

```

The following code snippent needs to be changed to the details of the configuration (json) file that is downloaded to the pc.

```
broker_address="lwm2m.mqtt.litmus.pro"                                                                 #broker address from the JSON
client1 = mqtt.Client("d836l5rd2pccboc9d3d1il7me")                                                     #client id or device id
client1.username_pw_set("d836l5rd2pccboc9d3d1il7me", "9ol0maf0kmar1jsvep8td81qhs")                     #username, password
client1.publish("loop/data/d836l5rd2pccboc9d3d1il7me/9snlwiy5sdbx64mn3wrn9remf/json",json_data,1)      #data topic to send data to.

```

The user will able to see data in the explore tab and the data values will change as per the datatype.

![alt text](https://bitbucket.org/litmusloopdocs/loopcloud-docs/raw/master/Demo/explore.png)


## INFO tab

In order to view the devie details in the **INFO** tab the user need to change the following object id data to the device data.

```
	json_data = json.dumps({"timestamp":0,"values":[{"objectId":3,"instanceId":0,"resourceId":0,"datatype":"String","value":"Litmus Automation"},
													{"objectId":3,"instanceId":0,"resourceId":1,"datatype":"String","value":"Model 1.0"}, 
													{"objectId":3,"instanceId":0,"resourceId":2,"datatype":"String","value":"Serial 1"},
													{"objectId":3,"instanceId":0,"resourceId":3,"datatype":"String","value":"Firmware 1.0"},
													random.choice(test)]})
```

The details for the individual parameters are as follows:
> |Description|Object id|Instance id|Resource id
> -------|-------------------|---------------|---------------
|**Manufacturer:** |Object id: 3|Instance Id: 0|Resource id: 0 |
|**Model Number:** |Object id: 3|Instance Id: 0|Resource id: 1 |
|**Serial Number:** |Object id: 3|Instance Id: 0|Resource id: 2 |
|**Firmware Version:**| Object id: 3|Instance Id: 0|Resource id: 3 |
