# Add a device to a project

Click on the Devices tab seen on the right side of the project's dashboard. It will open up the devices dashboard. 

The device daashboard conists of the device id, the date and time it is created, Type of the device, User defined description,Device Status, Status Time and the action to change the status of a device.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/devices_dashboard.png?token=93928d8fff210496e31d23f79eaf289d2f439580)

To add add a device, click on the **Single** tab shown on the top left of the page,next to **Demo Device**

A pop-up to select the model type (previously defined under Models by the user) will be displayed. Select the Model type to connect the device to the cloud.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/to_select_model_singledevice.png?token=d910236a5effeb84f448ebfbbe8f2cf6a924c59b)

Once selected the device with its parameters will be added to the list as shown below.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/device_added.png?token=749f3507e901a1b8ac1e79c6d0dfdb3550c0611d)

The Actions tab can be used to change the Status of a device

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/to_change_device_status.png?token=3b6d1fd3b0a7397728de571b851758d28922ae42)

By default the device state is **Active**

**BLOCK**- Device can be blocked to send any data to the server. ***NOTE***- Currently MQTT and REST protocols does not block the data and is enabled in POlling only.

**DELETE**-Device can be deleted. ***NOTE***- Delete status is changed in real time, but actual deletion takes place after 24 hours, so in case you deleted a device by mistake, you have a day to get it back online

**MAINTENANCE**-Device can be set to Maintenance mode if there is a physical maintenance to be performed on the device. ***NOTE***- Currently MQTT and REST protocols does not block the data and is enabled in POlling only.



