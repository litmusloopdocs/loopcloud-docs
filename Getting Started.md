# Getting Started

## STEP 1:

Click on **New user? Register**

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/image1.png)

## STEP 2:

Fill in the details shown on the sign up page and click submit

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/1.png)

## STEP 3:

An email with the verification link will be sent to you.Click on the link to verify the email. *NOTE: The link is only valid for 5 minutes*

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/a.PNG)

## STEP 4:

Click on the email and sign in with your username and password to log in

## STEP 5:

One can see the list of companies as you log in. **NEW** tab can be used to create a new company

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/3.png)

## STEP 6:

A form with the company details will open up. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/4.png)

Please fill in the appropriate details about the company.Below is an example of a company with details

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/5.png)

Once the company is created it is listen as shown below

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/6.png)

## STEP 7:

Click on the company to access the company parameters and to create a new project

**NOTE: It will allow you to access your locaiton, please click allow so that we can show you where in the world your device is located**

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/7.png)

The company dashboard will have the company parameters on the right tab as shown below

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/8.PNG)

## STEP 8:

Click on **Settings** to see the company settings. 

A User can be invited as a an *OWNER* by adding them through thier username.

Teams can be created with different owner and can be assigned previlages to read or write.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/9.png)

## STEP 9:

Click on the **Events** to see the events that happened in a company

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/10.png)

## STEP 10:

Click on **New Project** under **Project List** to create a new project.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/11.png)

A new form will open, where the details about the projects needs to be entered as shown in an example below

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/12.png)

Click create and a new project will be created as shown below. Multiple projects can be created inside a comapany.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/13.png)

## STEP 11:

CLick on the project to access the project settings and to add devices to the project

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/14.png)

The Dashboard shows the overview of the project on a single page. 

It shows what services are active on the server side. A Statistical visualizaiton of the messages, number of devices and alterts is shown here. 

The various setting of the project can be accessed from the tabs on the right side.

## STEP 12:

Click on Models to create a data model. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/15.png)

Standard models of services offered are created, and user can choose it or add thier own parameters.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/16.png)

Name the model and click Save 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/17.png)

The models will be saved with the specified name. Multiple models can be ceated with unique names.Filters can be applied to sort the models based on their types i.e MQTT,HTTP,LWM2M

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/17_a.png)

## STEP 13:

Click on **Devices** to add device to a project

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/18.png)

Click **Single** on the right top corner under deployment, a tab with the list of Models created wil appear, Select the Model you want to deploy.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/19.png)

The device will be deployed as shown below

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/20.png)

## STEP 14

Click on the device id to see the device properties

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/21.png)

Once the device starts sending data to the server the device parameters like the Manufacturer, Model Number, Serial Number, Firmware version is updated. 

Once can define Device Tags, and add description to the devices. 

Tags, can be used to select a group of devices with the same Tags and provide an over the air update or perform any group operation

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/22.png)

**NOTE** The data needs to be sent according to the JSON Schema as per the [OMNA Lightweight M2M (LWM2M) Object & Resource Registry](http://www.openmobilealliance.org/wp/registries.html)

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "timestamp": {
      "type": "integer"
    },
    "values": {
      "type": "array",
      "properties": {
        "objectId": {
          "type": "integer"
        },
        "instanceId": {
          "type": "string"
        },
        "resourceId": {
          "type": "integer"
        },
        "datatype": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "objectId",
        "instanceId",
        "resourceId",
        "datatype",
        "value"
      ]
    }
  },
  "required": [
    "values"
  ]
}

```

If the data is sent in the correct Schema as shown above then you can visualize the data under the **EXPLORE** tab

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/23.png)

Click on the Last Value to have a tabular view of the data along with the timestamp

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/24.png)

Charts are available next to the tabular view to have a graphical visualization of the data in neal real time. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/25.png)

**RAW** tab gives you all the raw data that is sent to the server.It will show data even if it is not sent in any JSON format at all. It is a great way to debug if your device is sending data to the server or not.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/26.png)

**LOG** shows the logs of the device, that is if the object is updated or added or deleted etc.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/27.png)

## STEP 15:

Click on the **Events** tab on the right side of the project dashboard to see the various events of the project.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/28.png)

Click on **Settings** tab on the right side of the project dashboard to see the settings of the project

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/29.png)

One can add the teams that were previously defined at a company level to the project and assing their privilege to *Read* or *Write*



The **Dashboard** is the place where you can visualize the summary of the project and also see if there are devices sending data

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Steps/b.png)












