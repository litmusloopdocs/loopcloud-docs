# MQTT TLS/SSL "Hello World Example"

Publish and subscribe using MQTT TLS/SSL using [Mqtt.fx](http://www.mqttfx.org/)

## STEP 1:

Create a model with **MQTT TLS/SSL connection paramteters** under a project

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/mqtts/model.png)

## STEP 2:

Deploy a device selectin the model that was created. A JSON will be downloaded which will have the server CA certificate.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/mqtts/devicedeployed.png)

## STEP 3:

Open Mqtt.fx and enter the details for the broker connection from the JOSN that was downloded.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/mqtts/mqttfx.png)

The Device status will change to online once successfull connection is established
*NOTE: The client ID should be the same as given in the configuration JSON that was downlooaded*

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/mqtts/onlinestatus.png)


## STEP 4:

Publish *Hello World* at the **mqttDataTopcName** that is given in the JSON downloaded and you can see the message in the RAW tab of the device 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/mqtts/HelloWorld.png)

