# Adding a new company

As soon as you login you will see the main screen to add companies.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/companies_page.png?token=a2417753ba531d6950a3f564bf7c0c50a164a9b0)

Click on New to add a company and provide the company details.

The domain name will be auto assigned as per the company name provided. Description for the company can also be provided to differentiate between multiple companies.

**NOTE:** Two companies cannot be created with the same name.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/create_company_form.png?token=2ce44318a79cbf282bb3cc31534865d0ba45efc9)

Once the company is created it will be shown in the list of companies as shown below.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/company_created.png?token=171d999ededfd8d6909cbf68821eddfa16e96bfc)

Multiple companies can be created by a user. Pagination helps the user to scroll between pages of companies.



