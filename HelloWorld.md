# Hello World using MQTT

User can see if the device is connected to **LOOP** by sending a simple "Hello World" message using **mosquitto**.

Mosquitto is a broker which allows user to publish and subscribe using command line.

*NOTE: Tested on linux based system*

## Installing mosquitto broker

Mosquitto can be installed using the following command

```
sudo apt-get install mosquitto mosquitto-clients

```
## Publsih "Hello World"

Any message can be publsihed from the command line using the configurations obtained in the **json** file that was downloaded when the device was deployed on Loop

```
mosquitto_pub -h "hostname" -p "port number" -u "username" -P "password" -m "Hello World" -t "topic"

```

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/HelloWorld/pub.png)

This command when executed will show a *"Hello Wrold"* message in the Raw of the device

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/HelloWorld/helloworld.png)


## Subscribe to a topic using command line

User can subscribe to the **mqttDataTopicName** which can be obtanied from the json file, to see what data the device is publishing. 

In the above case you will see "Hello world" as that is being published from the device to the topic.

```
mosquitto_sub -h "hostname -p "1883" -u "username" -P "password" -t "topic"

```
![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/HelloWorld/sub.png)


