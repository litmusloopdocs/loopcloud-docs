# Key-Value pair    

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/key-value/1.png)

The key-value tab is to define a key with multiple values. 

It is generally used in the POLL models, when we have one web service and we are going to call it multiple times with different parameters, we put the values in the Jey-value and use the key instead of the values.

With the Key-value, we do not need to write he actual logic for the device parameter but can change it with the help of pre-defined Key-Value pair. 
