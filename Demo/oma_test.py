import paho.mqtt.client as mqtt  #import the client1
import time
import json
import random

def on_connect(client, userdata, flags, rc):
    m="Connected flags"+str(flags)+"result code "\
    +str(rc)+"client1_id  "+str(client)
    print(m)
	

def on_message(client1, userdata, message):
    print("message received  "  ,str(message.payload.decode("utf-8")))

test = [{"objectId":3200,"instanceId":0,"resourceId":5500,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3201,"instanceId":0,"resourceId":5550,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3202,"instanceId":0,"resourceId":5600,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3203,"instanceId":0,"resourceId":5650,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3300,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,30)},
		{"objectId":3301,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,40)},
		{"objectId":3302,"instanceId":0,"resourceId":5500,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3303,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(10,1000)},
		{"objectId":3304,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(10,100)},
		{"objectId":3305,"instanceId":0,"resourceId":5800,"datatype":"float","value":random.uniform(10,100)},
		{"objectId":3306,"instanceId":0,"resourceId":5850,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3308,"instanceId":0,"resourceId":5900,"datatype":"float","value":random.uniform(0,50)},
		{"objectId":3310,"instanceId":0,"resourceId":5823,"datatype":"String","value":"Event identifier"},{"objectId":3310,"instanceId":0,"resourceId":5824,"datatype":"String","value":"Start time"},{"objectId":3310,"instanceId":0,"resourceId":5825,"datatype":"String","value":"minimum duration"},
		{"objectId":3311,"instanceId":0,"resourceId":5850,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3312,"instanceId":0,"resourceId":5850,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3313,"instanceId":0,"resourceId":5702,"datatype":"float","value":random.uniform(0,10)},{"objectId":3313,"instanceId":0,"resourceId":5703,"datatype":"float","value":random.uniform(0,10)},{"objectId":3313,"instanceId":0,"resourceId":5704,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3314,"instanceId":0,"resourceId":5702,"datatype":"float","value":random.uniform(0,50)},
		{"objectId":3315,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,40)},
		{"objectId":3316,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,30)},
		{"objectId":3317,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3318,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3319,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3320,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,30)},
		{"objectId":3321,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,40)},
		{"objectId":3322,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,50)},
		{"objectId":3323,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,60)},
		{"objectId":3324,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,50)},
		{"objectId":3325,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,40)},
		{"objectId":3326,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,30)},
		{"objectId":3327,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3328,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3329,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3330,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3331,"instanceId":0,"resourceId":5805,"datatype":"float","value":random.uniform(0,30)},
		{"objectId":3332,"instanceId":0,"resourceId":5705,"datatype":"float","value":random.uniform(0,40)},
		{"objectId":3333,"instanceId":0,"resourceId":5506,"datatype":"float","value":random.uniform(0,50)},
		{"objectId":3334,"instanceId":0,"resourceId":5702,"datatype":"float","value":random.uniform(0,20)},{"objectId":3334,"instanceId":0,"resourceId":5703,"datatype":"float","value":random.uniform(0,10)},{"objectId":3334,"instanceId":0,"resourceId":5704,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3335,"instanceId":0,"resourceId":5706,"datatype":"String","value":"RED"},
		{"objectId":3336,"instanceId":0,"resourceId":5514,"datatype":"String","value":"Latitude"},{"objectId":3336,"instanceId":0,"resourceId":5515,"datatype":"String","value":"Longitude"},
		{"objectId":3337,"instanceId":0,"resourceId":5536,"datatype":"float","value":random.uniform(0,10)},{"objectId":3337,"instanceId":0,"resourceId":5537,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3338,"instanceId":0,"resourceId":5850,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3339,"instanceId":0,"resourceId":5522,"datatype":"Opaque","value":"clip"},
		{"objectId":3340,"instanceId":0,"resourceId":5521,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3341,"instanceId":0,"resourceId":5227,"datatype":"String","value":"Test"},
		{"objectId":3342,"instanceId":0,"resourceId":5500,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3343,"instanceId":0,"resourceId":5851,"datatype":"float","value":random.uniform(0,10)},
		{"objectId":3344,"instanceId":0,"resourceId":5532,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3345,"instanceId":0,"resourceId":5500,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3346,"instanceId":0,"resourceId":5700,"datatype":"float","value":random.uniform(0,20)},
		{"objectId":3347,"instanceId":0,"resourceId":5500,"datatype":"Boolean","value":random.randint(0,1)},
		{"objectId":3348,"instanceId":0,"resourceId":5547,"datatype":"Integer","value":random.randint(0,100)}]

			
broker_address="lwm2m.mqtt.litmus.pro"					#Repalce with the URL from the JSON downloaded
client1 = mqtt.Client("4mf7ene61gilacs657tlpcptc")    	#Replace with the ClientId from the JSON downloaded
client1.on_connect= on_connect        					#attach function to callback
client1.on_message=on_message        					#attach function to callback
time.sleep(1)
client1.username_pw_set("4mf7ene61gilacs657tlpcptc", "kj1nvcho6nj7jftujm8l3e3t86") #Replace it with the username and password from the JSON, for eg client1.username_pw_set("username","password")
client1.connect(broker_address)      #connect to broker
client1.loop_start()    #start the loop
client1.subscribe("loop/data/d836l5rd2pccboc9d3d1il7me/9snlwiy5sdbx64mn3wrn9remf/json")
while True:
	json_data = json.dumps({"timestamp":0,"values":[{"objectId":3,"instanceId":0,"resourceId":0,"datatype":"String","value":"Litmus loop"},
													{"objectId":3,"instanceId":0,"resourceId":1,"datatype":"String","value":"Model 1.0"}, 
													{"objectId":3,"instanceId":0,"resourceId":2,"datatype":"String","value":"Serial 1"},
													{"objectId":3,"instanceId":0,"resourceId":3,"datatype":"String","value":"Firmware 1.0"},
													random.choice(test)]})
	print(json_data)
	client1.publish("loop/data/4mf7ene61gilacs657tlpcptc/91cg9b63dipq46ogw6ioyjo95/json",json_data,1)	#Replace with the mqttDataTopicName from the JSON downloaded
	time.sleep(5)
#client1.disconnect()
#client1.loop_stop()


