# Data values    

The data values send at particular objects and resources can be monitered in the Explore tab under devices. Near real data time data can de seen as with the timestamp

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/7d9291955a39e062f2ae47888d3b74ad4fa858d0/images/sensor_values.png?token=0dab7886afa66760de5d13b986cc67292b2aa471)

An overview of numeric data can be seen under the chart options next to the Table under Expplore tab.

This feature can be handy to see any abnormality at a glance in the device. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/7d9291955a39e062f2ae47888d3b74ad4fa858d0/images/graphical_visualizaiton_sensor_values.png?token=a057676b58a018fe2f6d2dbf0eff370940d4d83a)