# Sign up details

Provide the username, password, email address to sign up for the first time on loop cloud. 

A verification link will be sent to the email specified during the sign up.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/sign_up.png?token=d73e118cdf72830c5be040adad81a57a9e10fa2e)