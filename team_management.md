# Team Management

This document shows how to create custom teams and how to add users to the team. Specfic teams can be assigned to specific project. Each team can be given Read,Write or owner privileges.

## STEP 1: Go to company setting page to see the Team list options

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/company_setting.png)

## STEP 2: Multiple OWNERS can be invited to a company by the username. Due to security reason email invitation is not enabled and member can only be added using the litmus.pro username

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/team_owner.png)

Team member can be added using their username

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/user_invited.png)

The Team member will recive a notification email with the details as shown

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/Invitation_email.png)

The member will be shown as onboard once the email is recevied.

*Refresh the page if it continues to show pending Status*

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/user_onboard.png)

## STEP 3: Multiple teams can be created using the **NEW** button in the **Team list** tab

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/create_team_user.png)

The team will be added below the **OWNER** team with an edit button to edit the team name, and a delete button to delete a team

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/team_user_added.png)

User can invite another member to the teams created by the **OWNER**. Click on the team that is created to add members to that team. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/user_added_to_custom_team.png)

The process to add a team member to a user created team is similar to that of adding an **OWNER**.

The user receives a notification email indicating the team details to which they are added

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/custom_team_invitation_email.png)

The user can be removed from any team by the project **OWNER**. The member receive a notificaiton email on removal from a team.

![alt tex](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/user_removed_email.png)

## STEP 4: Click on the Projects Tab next to the company's tab in company setting. The Project tab consists of the list of Projects inside a company. 

Click on the project to add the team to it. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/team_management.png)

The Team list can be viewed in the drop down list and each team can be given the Read or Write privileges.

Multiple team can be added to a project with different actions, i.e. Read or Write

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/team_added_to_a_custom_project.png)

Write action can be assigned to a particular team so that they can make changes to the project that is add devices etc.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Team_management/write_previlage_to_team_user.png)

