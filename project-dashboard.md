# Project Dashboard

Click on the project created to add devices. A porject dashboard as shown below will show the diffenent statistics of the device.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/5a82029300c5c70d9b2dc3f4ef853a7def0042c5/images/project_dashboard.png?token=33c2ec5a5c4c806c3f9b4c37090f9b538ca01f08)

**Services Status**: Shows the protocols supported by Loop

**Messages**: Shows the graph of number of messages sent in near real time

**Usage Limits**: Shows the statistics of the devices, models and the alerts

Once devices are added and start to send the data, it can be visualize graphically as shown below.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/e1e30fa76d728faa422ec928824d03c2665a0cc8/images/messgaes_being_send_dashboard.png?token=15c4bd7669a4766e132e2d88b94aad225019c05c)