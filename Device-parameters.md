# Device Attributes

Click on device id to explore the device parameters. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/device_properties.png?token=9e2ac6c178f9e790768d975a21998b9da219ff59)

## INFO

The device resources will be updated once the device sends the data. Device description and Device Tags can be added by the user. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/device_info_updated.png?token=a755dfd072bc071256724d7e766c021324ca8b51)

## EXPLORE

The explore tab shows the resources that the device is sending to the server. The data needs to be send as per the oma specifications as shown in the schema which can be downloaded from the settings tab

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/7d9291955a39e062f2ae47888d3b74ad4fa858d0/images/schema.png?token=7003c4ce13f5f57d693b951f7326bde0550e9432)

The object id, Instance, Resouce will be updated as per the oma specifications. The Last value is updated in near real time and it also provied the date and time when the data was updated. 

The object id /3/0/0,/3/0/1,/3/0/2,/3/0/3  needds to be updated to see the device properties in the INFO tab.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/device_resources.png?token=6d6b89f4b855bedc70c2a49fdb2f3ef9f4b92e51)

## RAW

Raw messages that the device sends can be visualized in the Raw Tab of the device properties.***NOTE***- It will even be displayed if it is not send as per the schema.

This tab is used to debug a device. If the device is not sending any data means there is no communication setup and if u can see the data in the Raw tab and it does not update in the Explore tab then the JSON that the device is sending is in wrong format.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/raw_data.png?token=85178c17f8aaeb0dc92856b89360f1a328cdb2dd)

## LOG

The LOG tab, shows the logs of the device. If there is a change in the objectid, resource id, instance id etc by the user or at the server level, it can be monitored here. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/958414082d7aeeca4b58a075b280c96f6894b53c/images/device_logs.png?token=700244915e5274904f25cd2682318df02e414a60)









