# Auth

One can access the authentication methods under the **Auth** tab which can be found on the left side under each project.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/auth.png)

The user can select different authentications from the drop down list as shown

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/drop_down_types.png)

# Basic Auth

The basic authentication conists of username and password for the Credential Name

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/basic_auth.png)

# Header Auth

The header authentication consists of the header name and the value

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/header_auth.png)

# Query Key Auth

The Query key authentication contains the querykey and the value

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/Query_key_auth.png)

# OAuth 2.0

The OAuth conists of the Authentical URL and the Token URL along with the client id and password for the authentiation url.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Auth/OAuth2.png)