# Models

Models are the type of configuration models that can be used to configure a device. It can be HTTP,HTTPS,MQTT,MQTTS,Lwm2m,Lwm2m with DTLS.

Click on New Model to add a model.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/5a82029300c5c70d9b2dc3f4ef853a7def0042c5/images/model-list.png?token=01cf006cad234e3065ab58281a97b90fcf1dee22)

Select from the lisit of models provied, with predefined set. 

**NOTE:** User can also add its own parameter and value to the predefined set. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/288c7bfb25cb4091b6e8a7d8653c087aa3c055fa/images/create_modeltype_form.png?token=97445f0615cecf22d1b07b0f066b77272576f6e5)

Models need to have a name to differntiate from each other.A filter is provided for user to filter the models based on its type.

Once created the models will be displayed next to the New Tab with the name and description provided by the user.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/5a82029300c5c70d9b2dc3f4ef853a7def0042c5/images/model_created.png?token=edd448c21cb1cfbf958a51924ccff5b411dfebf8)


