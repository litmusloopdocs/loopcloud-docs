# Profile settings

Login to security.litmus.cloud to access the profile, in order to change password, setup two factor authentication, and get more details about your account profile.

## Account

Login to security.litmus.cloud and sign with the same user name and password you created for litmus.pro

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/Password.png)

## Password

User can edit, your email, First Name, Last Name under **Account** tab

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/account.png)

## Password

 User can change the password unde the **Password** tab

 ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/Password.png)

## Authenticator

 User can setup two step authentication from the **Authenticator** tab by following the instructions given on the page

 ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/authenticator.png)

## Sessions

 **Sessions** tab gives the details of the account which inlcudes the IP address, Time date and time the account was started, Last Access, when does it expires and the Clinets.

 ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/sessions.png)

## Applications

 User can know the details of the applications which they have access too and what are the permissions they are granted.

 ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/applications.png)

## Log

 User can see the account related logs on the **Log** tab

 ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/Profile/log.png)