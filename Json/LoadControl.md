# Load Control

```
{
	"timestamp": 0,
	"values": [
		{
			"objectId": 3310,
			"instanceId": 0,
			"resourceId": 5823,
			"datatype": "String",
			"value": "Event identifier"
		},
		{
			"objectId": 3310,
			"instanceId": 0,
			"resourceId": 5824,
			"datatype": "String",
			"value": "Start Time"
		},
		{
			"objectId": 3310,
			"instanceId": 0,
			"resourceId": 5825,
			"datatype": "String",
			"value": "Duration in minimum"
		}
	]
}

```