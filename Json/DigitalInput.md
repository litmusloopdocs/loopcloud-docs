# Digital Input 

Generic digital input for non-specific sensors



```
{
	"timestamp": 0,
	"values": [
		{
			"objectId": 3200,
			"instanceId": 0,
			"resourceId": 5500,
			"datatype": "Boolean",
			"value": 1
		}
	]
}
```