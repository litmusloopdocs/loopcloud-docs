# Analogue Input

Generic analog input for non-specific sensors

```
{
	"timestamp": 0,
	"values": [
		{
			"objectId": 3202,
			"instanceId": 0,
			"resourceId": 5600,
			"datatype": "Float",
			"value": 10.2
		}
	]
}
```