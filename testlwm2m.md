# Test Lwm2m using mozilla plugin

Lwm2m can be easily rested with a addon from Mozilla firefox browser. Click [here](https://addons.mozilla.org/en-US/firefox/addon/oma-lwm2m-devkit/?src=ss) to add the add on to the browser.

Follow the Steps below to connect an example client to the LOOP platform

## STEP 1:

Click on the oma icon ![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Demo/icon.PNG) seen on the toolbar.

## STEP 2:

Enter the lwm2m URL that you get from the JSON when you deploy a lwm2m device in loop in the dialog box that appears, as shown below.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Demo/url.png)

## STEP 3:

A dashobard as shown below will open up once you enter the URL and proceed.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Demo/dashboard.PNG)

Click on *Load Lwm2m Client* and select *Example Client*

Click on *Client Registration* and enter the *Endpoint Client Name* to register the client to LOOP

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Demo/endpoint.png)

Define the Lifetime of the client for which you want it to be active

# STEP 4:

The Device is registered and sends the data sources that are defined in the example client

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master//Demo/registered.png)

