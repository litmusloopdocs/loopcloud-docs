# Company Settings 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/1.png)

Company Desciption can be changed from the company settings tab. A 150 word limit allow you to add detail description of the company

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/company_description.png)

A user can be invited to the company as an *OWNER* or to a team with READ/WRITE previledge as assigned to the team.

## Steps to add a user as an OWNER

### STEP 1: Add any user with an account on Loop 2.0 by their username. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/2.png)

### STEP 2: User will be added with a status of pending as shown

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/add_user_company.png)

### STEP 3: The user will receive an email with a link of the company to accept the invitation

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/company_invitation.PNG)

### STEP 4: User status will change from pending to onboard

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/onboard.png)

## Steps to add user as a Team Member

### STEP 1: Create a new team by clicking *NEW*

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/new_team.png)

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/3.png)

### STEP 2: Click on the team created to invite users to that particular team

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/4.png)

### STEP 3: Add any user with an account on Loop 2.0 by their username

### STEP 4: User will be added with a status of pending as shown

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/add_user_team.png)

### STEP 5:The user will receive an email with a link of the team to accept the invitation

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/settings/team_invite.PNG)

### STEP 6: User status will change from pending to onboard

