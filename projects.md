# Creating your first project

Click on the company name to enter the dashboard. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/company_dashboard.png?token=cd9f7abb108d90b85d9dbb65fbdf830e7c905b57)

Click on New project to make your project. Multiple projects with description can be created under a single company. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/create_project_form.png?token=3cf99517b55310fc61010ee52c2e774dfac2a058)

Once the project is created it can be seen next to the New Project Tab, with the name and the description provided by the user.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/3449bdc55cb24ccdd4906d173d553cc87e248b08/images/project_created.png?token=fe0293ab13f7729e37accadbb8b2a51feb7b8b4b)
