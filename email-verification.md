# Verification

An email with a verification link will be send to you after successful sign up.
Click on the link received in the email to verify for account.

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/images/email_for_verification.PNG)



**NOTE:** The verification link will expire in 5 mins. One can resend the verification link from the resend link as shown in the figure below
![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/images/email_verification.png?token=72637b638e7f301eb4b5b724ccac81b588330073)

Clinking the verification link will verify the account and a confirmation of verification will be displayed. 

![alt text](https://bytebucket.org/litmusloopdocs/loopcloud-docs/raw/master/images/email_verified.PNG?token=0b367595b1a46e3c98f9ecbb3b9ee30891435d73)